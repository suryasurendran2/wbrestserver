package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.ExecuteSqlCmdMst;


public interface ExecuteSqlCmdMstRepository extends JpaRepository<ExecuteSqlCmdMst, String>{
	
	List<ExecuteSqlCmdMst> findByExecuteStatus(java.lang.String string);

}
