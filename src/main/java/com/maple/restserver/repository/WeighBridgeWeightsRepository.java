package com.maple.restserver.repository;

 
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.WeighBridgeWeights;


@Component
@Repository
public interface WeighBridgeWeightsRepository extends JpaRepository<WeighBridgeWeights, String>{
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where "
			+ "lower(vehicleno) like :vno and (nextweight is null or nextweight=0) "
			+ " and (status is null or status='INCOMPLETE'  "
			+ " order by voucher_date desc")
	List<WeighBridgeWeights> searchByVehicleNoWithSecndWtNull(String vno);
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where "
			+ "lower(vehicleno) like :vno AND  status = 'INCOMPLETE' order by voucher_date desc")
	List<WeighBridgeWeights> searchByVehicleNo(String vno);

	List<WeighBridgeWeights> findByVehicleno(String vno);
	 
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where  "
			+ "  vehicleno = :VehicleNo "
			+ " and date(voucher_date) >= :vdate and status='INCOMPLETE' and  "
			+ " ft_vehicleload_status <> :vehicleloadstatus"
			+ " order by voucher_date DESC ")
	List<WeighBridgeWeights> getListOfFirstWeightOfVehicle(Date vdate,String VehicleNo, String vehicleloadstatus);
	
	@Query(nativeQuery = true,value = "select  * from weigh_bridge_weights h  where h.company_mst=:companymstid and  date(h.voucher_date)=:fdate   ")
	List<WeighBridgeWeights>getWeighBridgeReport( String  companymstid ,Date fdate );
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where (nextweight is null or nextweight=0)")
	List<WeighBridgeWeights> getWeighBridgeWithSecondWtNull();

	
	
	
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where (date(voucher_date)>=:fdate OR date(snd_voucher_date)>=:fdate) AND (date(snd_voucher_date)<=:tdate OR date(voucher_date)>=:tdate)")
	List<WeighBridgeWeights> findByweighBridgeWeightBetweenDate(Date tdate, Date fdate);

	
	//==============MAP-229-show-all-weigh-bridge-details-based-on-a-single-day===========anandu=========18-10-2021========
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where date(voucher_date)=:stdDate")
	List<WeighBridgeWeights> findByDate(Date stdDate);
	
	
	
 
	 
	
	//,Date tDate
	//,Date tDate
	
	//date(h.voucher_date) >= :fromDate and date(h.voucher_date) <= :tDate

	//and h.voucher_date <=:tdate
//and date(h.voucher_date) <= :tDate 
}