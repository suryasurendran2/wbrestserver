package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ParamValueConfig;

@Repository
public interface ParamValueConfigRepository extends JpaRepository<ParamValueConfig,String>{

	
	ParamValueConfig findByParamAndCompanyMst(String param,CompanyMst companyMst);

	List<ParamValueConfig> findByCompanyMst(CompanyMst companyMst);

}
