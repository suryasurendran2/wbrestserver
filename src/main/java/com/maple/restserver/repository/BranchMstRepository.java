package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;


@Component
@Repository
public interface BranchMstRepository extends JpaRepository<BranchMst, String>{

	
	
	/* @Query(nativeQuery=true,value=" SELECT * FROM Branch_Mst b WHERE b.My_Branch = 'N'  ") */
	
	List<BranchMst> findByMyBranch(String myBrach);
	 
	 
	 @Query(nativeQuery=true,value=" SELECT * FROM Branch_Mst b WHERE b.My_Branch = 'Y'  ") 
		BranchMst getMyBranch();
		 
	 
	 @Query(nativeQuery=true,value=" SELECT * FROM Branch_Mst b WHERE b.My_Branch = 'Y'  ") 
	List<BranchMst> getMyBranchList();
	 
	 BranchMst findByBranchName(String branchName);
	 
	 
	 Optional<BranchMst>findByBranchCodeAndCompanyMstId(String branchCode,String companymstid);
	 
	 List<BranchMst>findByCompanyMstId(String companymstid);
	
	 List<BranchMst> findByCompanyMst(CompanyMst companyMst);


	BranchMst findByBranchNameAndCompanyMst(String branchname, Optional<CompanyMst> companyMstOpt);
	 
	 List<BranchMst>  findByBranchCode(String branchCode);

	
}
