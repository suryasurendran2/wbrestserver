package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.WeighBridgeVehicleMst;


@Component
@Repository
public interface WeighBridgeVehicleMstRepository extends JpaRepository<WeighBridgeVehicleMst, String>{

	WeighBridgeVehicleMst findByVehicletype(String vehicletype);
	
	

}
