package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.WeighBridgeMaterialMst;

 
@Component
@Repository
public interface WeighBridgeMaterialMstRepository extends JpaRepository<WeighBridgeMaterialMst, String>{


	 WeighBridgeMaterialMst  findByMaterial(String material);
	
	
 
	

}
