package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TyreWeightSavingMst;

@Component
@Repository
public interface TyreWeightSavingMstRepository extends JpaRepository<TyreWeightSavingMst, String> {

	List<TyreWeightSavingMst> findByCompanyMst(CompanyMst companyMst);

}
