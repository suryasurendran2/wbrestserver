package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.WeighingItemMst;




@Repository
public interface WeighingItemMstRepository extends JpaRepository<WeighingItemMst, String> {
	
	WeighingItemMst findByCompanyMstIdAndBarcode(String companymstid,String barcode);

	WeighingItemMst findByCompanyMstIdAndItemId(String companymstid,String itemId);
	
	
}
