package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.WeighBridgeVehicleTypeMst;



@Component
@Repository
public interface WeighBridgeVehicleTypeRepository extends JpaRepository<WeighBridgeVehicleTypeMst, String>{

	WeighBridgeVehicleTypeMst findByVehicleno(String vehicleNo);

	Optional<WeighBridgeVehicleTypeMst> findByCompanyMstAndVehicleno(CompanyMst companyMst, String vehiclenumber);
	
	
	
	
}
