package com.maple.restserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

//import org.apache.derby.drda.NetworkServerControl;
import org.apache.derby.drda.NetworkServerControl;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
//import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DBMaintMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.service.DBMaintMstService;

// Uncomment following to stop Spring Deploy of camunda Process

//@EnableProcessApplication

@EnableScheduling
@SpringBootApplication
public class RestserverApplication {

	// private static final Logger logger =
	// Logger.getLogger(ApplicationStartupRunnerOne.class);
	private static final Logger logger = LoggerFactory.getLogger(RestserverApplication.class);
//	public static ApplicationContext applicationContext;
	public static Connection localCn;

	@Value("${migrationdb}")
	private String migrationdb;

	@Autowired
	DBMaintMstService dbMaintMstService;

	@Autowired
	FinancialYearMstRepository financialYearMstRepository;

	@Autowired
	SysDateMstRepository sysDateMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@Value("${mycompany}")
	private String mycompany;
	@Value("${mybranch}")
	private String mybranch;

//	EventBus eventBus = EventBusFactory.getEventBus();

	ArrayList<Map> arrayOfHashMap = new ArrayList();

//	@Autowired
//	private RuntimeService runtimeService;

	public static void main(String[] args) {

		// Logger.getRootLogger().setLevel(Level.DEBUG);

		// System.setProperty("javax.net.ssl.trustStore","clientTrustStore.key");

		try {

			String currentDirectory = System.getProperty("user.dir");
			Properties p = System.getProperties();

			p.setProperty("derby.system.home", currentDirectory);
			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("localhost"), 1529);

			server.start(null);

			logger.info("DB Server Started");
		} catch (Exception e) {
			logger.info("DB Server could not be Started");
		}

		// Uncomment following for Data transfer.

		SpringApplication.run(RestserverApplication.class, args);
		// synchWithServer();

	}

	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

		PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
		properties.setLocation(new FileSystemResource("application.properties"));
		properties.setIgnoreResourceNotFound(false);

		return properties;
	}

	@Bean
	public Connection getConnection() {
		try {

			// RestserverApplication.localCn = getDbConn("SOURCEDB");
			RestserverApplication.localCn = getDbConn(migrationdb);
			if (null == RestserverApplication.localCn) {
				RestserverApplication.localCn = getDbConn2(migrationdb);

			}

		} catch (UnknownHostException e) {

			logger.error(e.getMessage());

		} catch (Exception e) {

			logger.error(e.getMessage());
		}
		return RestserverApplication.localCn;
	}

	@EventListener(ApplicationReadyEvent.class)
	public String doSomethingAfterStartup() {

		// -------------sharon ------------------by using DBMaintMst------------
		String id = "SQL01";
		String sqlString = "alter table sales_trans_hdr modify voucher_type varchar(50)";
		String execStatus = "NO";

//		alter table sales_trans_hdr modify voucher_type varchar(50);

		try {
			ArrayList<DBMaintMst> dbMaintMstList = new ArrayList<DBMaintMst>();
			DBMaintMst dbMaintMst = new DBMaintMst();
			dbMaintMst.setId("SQL01");
			dbMaintMst.setSqlString(
					"insert into weigh_bridge_vehicle_mst(id,vehicletype,rate,company_mst,branch_mst) values('1','4','50','WB','WB')");
			dbMaintMst.setExecStatus("NO");
			dbMaintMstList.add(dbMaintMst);

			DBMaintMst dbMaintMst1 = new DBMaintMst();
			dbMaintMst1.setId("SQL02");
			dbMaintMst.setSqlString(
					"insert into weigh_bridge_vehicle_mst(id,vehicletype,rate,company_mst,branch_mst) values('1','8','50','WB','WB')");
			dbMaintMst1.setExecStatus("NO");
			dbMaintMstList.add(dbMaintMst1);

			DBMaintMst dbMaintMst2 = new DBMaintMst();
			dbMaintMst2.setId("SQL02");
			dbMaintMst2.setSqlString(
					"insert into weigh_bridge_vehicle_mst(id,vehicletype,rate,company_mst,branch_mst) values('1','12','50','WB','WB')");
			dbMaintMst2.setExecStatus("NO");
			dbMaintMstList.add(dbMaintMst2);

			for (DBMaintMst dbMaint : dbMaintMstList) {

				dbMaintMstService.insertDBmaintmst(dbMaint.getId(), dbMaint.getSqlString(), dbMaint.getExecStatus());
			}

		} catch (Exception e) {

			logger.error(e.getMessage());

		}
//		alter table sales_trans_hdr modify voucher_type varchar(50);

		String returnstatus;

		System.out.println("hello world, I have just started up1");
		logger.error("hello world, I have just started up2");
		logger.info("hello world, I have just started Info");

//		eventBus.register(this);

		returnstatus = "ITEMBATCHDTLNODONEANDFINDONEEVENTREGITER";
		/*
		 * try { Process proc =
		 * Runtime.getRuntime().exec("java -jar mapleclientv2.jar"); } catch
		 * (IOException e) { logger.error(e.getMessage()); }
		 */

		logger.info("ApplicationStartupRunnerOne run method Started !!");
		logger.debug("DEbugging");

		return returnstatus;
	}

	// receipt tally

	// payment tally coding

	public static Connection getDbConn(String dbName) throws UnknownHostException, Exception {

		// jdbc:derby:d:/myproject/projectDB;create=true;user=user1;password=psssword

		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Connection conn = null;
		// String password = "thisisthepassword913781";

		String password = "male04";

		Class.forName(driver).newInstance();

		try {

			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("localhost"), 1527);
			server.start(null);
			while (true) {
				Thread.sleep(500);
				try { // server.ping();
					break;
				} catch (Exception e) {
				}
			}

			Properties props = new Properties(); // connection properties

			props.put("user", "maple04");
			props.put("password", password);
			// String dbName = dbNamewithoutDB + "DB"; // the name of the database

			String url = "";

			url = "jdbc:derby:" + dbName + ";create=false";

			conn = DriverManager.getConnection(url, props);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				logger.error(e.getMessage());
			}

			System.out.println("Connected to local db and created database " + dbName);

			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			conn.setAutoCommit(true);

		} catch (SQLException sqle) {
			System.out.println("Error Connecting Client DB");
			return null;
		}

		return conn;

	}

	public static Connection getDbConn2(String dbName) throws UnknownHostException, Exception {

		// jdbc:derby:d:/myproject/projectDB;create=true;user=user1;password=psssword

		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Connection conn = null;
		// String password = "thisisthepassword913781";

		String password = "thisisthepassword913781";

		Class.forName(driver).newInstance();

		try {

			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("localhost"), 1527);
			server.start(null);
			while (true) {
				Thread.sleep(500);
				try { // server.ping();
					break;
				} catch (Exception e) {
				}
			}

			Properties props = new Properties(); // connection properties

			props.put("user", "xposdemo");
			props.put("password", password);
			// String dbName = dbNamewithoutDB + "DB"; // the name of the database

			String url = "";

			url = "jdbc:derby:" + dbName + ";create=false";

			conn = DriverManager.getConnection(url, props);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				logger.error(e.getMessage());
			}

			System.out.println("Connected to local db and created database " + dbName);

			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			conn.setAutoCommit(true);

		} catch (SQLException sqle) {
			System.out.println("Error Connecting Client DB");
			return null;
		}

		return conn;
	}

	/*
	 * @Order(value = 3)
	 * 
	 * @Component class ApplicationStartupRunnerOne implements CommandLineRunner {
	 * 
	 * @Override public void run(String... args) throws Exception {
	 * 
	 * 
	 * logger.info("ApplicationStartupRunnerOne run method Started !!");
	 * 
	 * //BranchMst branchMst = branchMstRepo.getMyBranch();
	 * ///synchWithServer(branchMst); } }
	 */

	public byte[] getbackupEntity() throws IOException {

		DateFormat df = new SimpleDateFormat("ddMMyy");
		Date dateobj = new Date();

		String fileName = df.format(dateobj);
		fileName = "./backup/" + fileName + ".zip";

		byte[] barray = Files.readAllBytes(Paths.get(fileName));

		return barray;
	}

}
