package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TyreWeightSavingMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.TyreWeightSavingMstRepository;

@RestController
public class TyreWeightSavingMstResource {
	
	@Autowired
	TyreWeightSavingMstRepository tyreWeightSavingMstRepository; 
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	
	//save 
		@PostMapping("{companymstid}/tyreweightsavingresource/savetyreweight")
		public TyreWeightSavingMst createTyreWeightSaving(
				@PathVariable(value="companymstid")	String companymstid,
				@Valid @RequestBody TyreWeightSavingMst tyreWeightSavingMst)
		{
			CompanyMst companymst=companyMstRepo.findById(companymstid).get();
			tyreWeightSavingMst.setCompanyMst(companymst);
			tyreWeightSavingMst=tyreWeightSavingMstRepository.save(tyreWeightSavingMst);
			return tyreWeightSavingMst;
		}
		
		
		// delete
		@DeleteMapping("{companymstid}/tyreweightsavingresource/deletetyreweightsaving/{id}")
		public void DeleteTyreWeightSaving(@PathVariable(value = "id") String Id) {
			tyreWeightSavingMstRepository.deleteById(Id);
		}
		
		
		//showing all 
		@GetMapping("{companymstid}/tyreweightsavingresource/showalltyreweightsaving")
		public List<TyreWeightSavingMst> showallTyreWeightSaving(
				
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
			
			return tyreWeightSavingMstRepository.findByCompanyMst(companyMstOpt.get());
		}
		
}
