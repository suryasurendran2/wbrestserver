package com.maple.restserver.resource;

 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
@Transactional
public class CompanyMstResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("/companymst")
	public CompanyMst createaCompanyMst(@Valid @RequestBody CompanyMst companyMst)
	{
		
		
	CompanyMst savedCompany = 	companyMstRepo.saveAndFlush(companyMst);
		
	
		
			
		return savedCompany;
 
		}

	@GetMapping("/companymst/{companymstid}/companymst")
	public Optional<CompanyMst> retrievCompanyMstById(@PathVariable String companymstid){
		return companyMstRepo.findById(companymstid);
	}
	@GetMapping("/getcompanymst/{companymstid}")
	public Optional<CompanyMst> getCompanyMstById(@PathVariable String companymstid){
		return companyMstRepo.findById(companymstid);
	}
 
 
	@GetMapping("/companymst")
	public List<CompanyMst> getAllCompanyMst(){
		return companyMstRepo.findAll();
	}
 

	@GetMapping("/{companymstid}/companymst")
	public List<CompanyMst> getAllCompanyMstForSystem(){
		return companyMstRepo.findAll();
	}
	
	@GetMapping("/{companymstid}/companymst/getversion")
	public String getVersion(){
		return ClientSystemSetting.version;
	}
	@GetMapping("/{companymstid}/companymst/getcountry")
	public List<String> getAllCountries()
	{
		
		ArrayList<String> strList = new ArrayList();
		strList.add("INDIA");
		strList.add("MALDIVES");
		return strList;
		
	}
	
//	@GetMapping("/{companymstid}/companymst/getstatebycountry")
//	public List<String> getAllStateByCountry(
//			@RequestParam (value ="country")String country)
//	{
//			ArrayList<String> strList = new ArrayList();
//			strList =	companyMstService.getStateByCountry(country);
//			return strList;
//
//		
//	}
	
}
