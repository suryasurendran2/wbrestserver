package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.WeighBridgeMaterialMst;
import com.maple.restserver.entity.WeighBridgeVehicleTypeMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.WeighBridgeVehicleTypeRepository;

@RestController
public class WeighBridegeVehicleTypeResource {
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	WeighBridgeVehicleTypeRepository weighBridgeVehicleTypeRepository;

	@PostMapping("{companymstid}/saveweighbridgevehicletypemst")
	public WeighBridgeVehicleTypeMst createWeighBridgeVehicleTypeMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst)
	{
		return companyMstRepository.findById(companymstid).map(companyMst-> {
			weighBridgeVehicleTypeMst.setCompanyMst(companyMst);
		
		
			return weighBridgeVehicleTypeRepository.save(weighBridgeVehicleTypeMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		

	}
	
	
	@GetMapping("{companymstid}/findweighbridgevehicletypemstbyvehiclenumber/{vehiclenumber}")
	public  Optional<WeighBridgeVehicleTypeMst>  retrieveWeighBridgeVehicleTypeMstByVehicleNumber(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vehiclenumber") String vehiclenumber){
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		return weighBridgeVehicleTypeRepository.findByCompanyMstAndVehicleno(companyMstOpt.get(),vehiclenumber);
	}
	
	

}
