package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.SystemSetting;
import com.maple.restserver.entity.WeighBridgeVehicleMst;
import com.maple.restserver.entity.WeighBridgeVehicleTypeMst;
import com.maple.restserver.entity.WeighBridgeWeights;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.WeighBridgeVehicleTypeRepository;
import com.maple.restserver.repository.WeighBridgeWeightsRepository;
import com.maple.restserver.service.WeighBridgeReportService;

@RestController
@Transactional
public class WeighBridgeWeightsResource {
	@Autowired
	private WeighBridgeWeightsRepository weighBridgeWeightsRepository;

	@Autowired
	WeighBridgeReportService  weighBridgeReportService;
	
 
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@Autowired
	WeighBridgeVehicleTypeRepository weighBridgeVehicleTypeRepository;

	@PostMapping("{companymstid}/saveweighbridgeweights")
	public WeighBridgeWeights createWeighBridgeWeights(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody WeighBridgeWeights weighBridgeWeights) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			weighBridgeWeights.setCompanyMst(companyMst);

			
			 
			if(null==weighBridgeVehicleTypeRepository.findByVehicleno(weighBridgeWeights.getVehicleno())) {
			 
				WeighBridgeVehicleTypeMst  weighBridgeVehicleTypeMst = new WeighBridgeVehicleTypeMst();
				
				weighBridgeVehicleTypeMst.setBranchMst(weighBridgeWeights.getBranchMst());
				weighBridgeVehicleTypeMst.setCompanyMst(weighBridgeWeights.getCompanyMst());
				weighBridgeVehicleTypeMst.setVehicleno(weighBridgeWeights.getVehicleno());
				weighBridgeVehicleTypeMst.setVehicletype(weighBridgeWeights.getVehicletypeid());
				weighBridgeVehicleTypeRepository.save(weighBridgeVehicleTypeMst);
			
			}
			
			
			return weighBridgeWeightsRepository.save(weighBridgeWeights);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}
	
	@GetMapping("{companymstid}/getfistweight/{vehicleno}/{vehicleloadstatus}")
	public WeighBridgeWeights getFirstWeightOfVehicle(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vehicleno") String vehicleno,
			@PathVariable(value = "vehicleloadstatus") String vehicleloadstatus){
		
 
				long DAY_IN_MS = 1000 * 60 * 60 * 24;
		Date twodayBefore = new Date(System.currentTimeMillis() - (5 * DAY_IN_MS));
		
		List<WeighBridgeWeights>  listOfFirstWeights = 
				weighBridgeWeightsRepository.getListOfFirstWeightOfVehicle(twodayBefore , vehicleno,vehicleloadstatus);
		
		WeighBridgeWeights weighBridgeWeights = null;
		if(listOfFirstWeights.size()>0) {
			weighBridgeWeights= listOfFirstWeights.get(0);
		} 
		
		
		
		
		return weighBridgeWeights;
	}

	@GetMapping("{companymstid}/getallweight")
	public List<WeighBridgeWeights> getAllWeightOfVehicle(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "data") String data){
		data = data.replaceAll("20%", " ");
		
		return weighBridgeWeightsRepository.searchByVehicleNo("%"+data.toLowerCase()+"%");
		
	}
	
	@GetMapping("{companymstid}/weighingbridgeweight/getweighbridgewithsecndwtnull")
	public List<WeighBridgeWeights> getAllWeightOfVehicleWithSecondWtNull(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "data") String data){
		data = data.replaceAll("20%", " ");
		
		return weighBridgeWeightsRepository.searchByVehicleNoWithSecndWtNull("%"+data.toLowerCase()+"%");
		
	}
	@GetMapping("{companymstid}/weighbridgeweights/{id}")
	public Optional<WeighBridgeWeights> getWeighBridgeWeightById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id){
		
		return weighBridgeWeightsRepository.findById(id);
		
	}
	
//	@GetMapping("{companymstid}/weighbridgeweights/weighbridgeweightbyvehiclenosearch")
//	public Optional<WeighBridgeWeights> getWeighBridgeWeightByVehicleNo(
//			@PathVariable(value = "companymstid") String companymstid,
//			@RequestParam(value = "data") String data){
//		
//		return weighBridgeWeightsRepository.searchByVehicleNo(vno)
//		
//	}
	
	@DeleteMapping("{companymstid}/weighbridgeweights/deletebyid/{id}")
	public void deleteWeighBridgeWts(@PathVariable(value = "id") String id)
	{
		weighBridgeWeightsRepository.deleteById(id);
	}
	

	
	

	@GetMapping("{companymstid}/weighbridgereportresurce/weighbridgereportbydate")
	public List<WeighBridgeWeights> getWeighBridgeReportBySingleDate(
			@PathVariable(value = "companymstid") String companymstid,
        @RequestParam("date") String date){
		
		
		
		
		java.util.Date udate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");
		
		return	weighBridgeWeightsRepository.getWeighBridgeReport( companymstid, udate);


	}	
	
	@GetMapping("{companymstid}/weighbridgereportresurce/getweighbridgewtswithsecondwtnull")
	public List<WeighBridgeWeights> getWeighBridgeWtsWithNullSecondWt()
	{
	
		return weighBridgeWeightsRepository.getWeighBridgeWithSecondWtNull();
	}
	@PutMapping("{companymstid}/weighbridgereportresurce/upateweighbridgeweightbyid")
	public void updateWeighBridgeWts(@PathVariable(value = "companymstid") String companymstid,
			@RequestBody WeighBridgeWeights weighBridgeWeightsreq)
	{
		WeighBridgeWeights weighBridgeWeights = weighBridgeWeightsRepository.findById(weighBridgeWeightsreq.getId()).get();
		
		weighBridgeWeights.setSecondVoucherNumber(weighBridgeWeightsreq.getSecondVoucherNumber());
		weighBridgeWeights.setNextweight(weighBridgeWeightsreq.getNextweight());
		weighBridgeWeights.setSecondCashToPay(weighBridgeWeightsreq.getSecondCashToPay());
		weighBridgeWeights.setSecondPaidAmount(weighBridgeWeightsreq.getSecondPaidAmount());
		weighBridgeWeights.setSecondRate(weighBridgeWeightsreq.getSecondRate());
		weighBridgeWeights.setSecondWeightdate(weighBridgeWeightsreq.getSecondWeightdate());
		weighBridgeWeights.setStatus(weighBridgeWeightsreq.getStatus());
		
		weighBridgeWeightsRepository.save(weighBridgeWeights);
	}
	
	@GetMapping("{companymstid}/getvehicledetailbyvehicleno")
	public List<WeighBridgeWeights> getvehicledetailbyvehicleno(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "vehicleno") String vehicleno){
	 
		List<WeighBridgeWeights>  listOfFirstWeights = 
				weighBridgeWeightsRepository.findByVehicleno( vehicleno);
		
		WeighBridgeWeights weighBridgeWeights = null;
		if(listOfFirstWeights.size()>0) {
			weighBridgeWeights= listOfFirstWeights.get(0);
		} 
		
	 		return listOfFirstWeights;
	}
	
	//==============MAP-229-show-all-weigh-bridge-details-based-on-a-single-day===========anandu=========18-10-2021=========
	@GetMapping("{companymstid}/weighbridgeweightsresource/getweighbridgedetails")
	public List<WeighBridgeWeights> getWeighBridgeDetails(
			@PathVariable("companymstid") String companymstid,
			@RequestParam("selecteddate") String selectedDate){
	 
		java.util.Date stdDate = SystemSetting.StringToUtilDate(selectedDate, "yyyy-MM-dd");
		return weighBridgeWeightsRepository.findByDate(stdDate);
		
	}
}



