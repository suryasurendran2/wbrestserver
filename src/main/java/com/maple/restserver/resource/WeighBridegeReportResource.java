package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.SystemSetting;
import com.maple.restserver.entity.WeighBridgeWeights;
import com.maple.restserver.repository.WeighBridgeWeightsRepository;
import com.maple.restserver.service.WeighBridgeReportService;


@RestController
public class WeighBridegeReportResource {

	@Autowired
	WeighBridgeWeightsRepository weighBridgeWeightsRepository;
	
	
	@GetMapping("{companymstid}/weighbridegereportresource/weighbridegereport")
	public List<WeighBridgeWeights> getWeighBridgeDetails(
			@PathVariable("companymstid") String companymstid,
			@RequestParam("fromdate") String fromDate,
			@RequestParam("todate") String toDate
			 ){

		java.util.Date fdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		return weighBridgeWeightsRepository.findByweighBridgeWeightBetweenDate(fdate, tdate );

	}
	
	
	

}
