package com.maple.restserver.resource.vouchernumber;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

 

/**
 * Invoice generation controller.
 */
@Controller
public class VoucherNumberController
{
  @Autowired
  private VoucherNumberService voucherService;

  /**
   * Generates a new invoice.
   */
  @RequestMapping(method = RequestMethod.GET, produces = "text/html", value = "/{companymstid}/vouchernumber")
  @ResponseBody
  public String invoice(@PathVariable(value = "companymstid") String
		  companymstid , @RequestParam(name="id") String id)
  {
	  
	  if(null==companymstid) {
		  companymstid="COM";
	  }
	 // id =id+companymstid;
	  
    final VoucherNumber voucherNo = voucherService.generateInvoice(id,companymstid);

    return  voucherNo.getCode();
		
  }
}
