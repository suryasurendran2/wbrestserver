package   com.maple.restserver.resource.vouchernumber.service;

 
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Business logic operations related to billing.
 */
@Service

@Component
public class VoucherNumberService
{
  @Autowired
  private VoucherCodeGenerator voucherCodeGenerator;

  @Autowired
  private VoucherNumberRepository voucherNumberRepository;

 
  /**
   * Generates an invoice.
   *
   * @return The generated {@link Invoice}.
   */
  @Retryable(backoff = @Backoff(delay = 2000, multiplier = 2, random = true), maxAttempts = 40)
  public VoucherNumber generateInvoice(String uCode, String companyId)
  {
	  //companyId
	  //String.format("%06d", voucherCodeGenerator.next(uCode)) 
	  
	  return voucherNumberRepository.save(new VoucherNumber(uCode + String.format("%06d", voucherCodeGenerator.next(companyId+uCode)), companyId));
   // return voucherNumberRepository.save(new VoucherNumber(  uCode +String.format("%06d", voucherCodeGenerator.next(uCode))   ));
  }
  
  
  @Retryable(backoff = @Backoff(delay = 2000, multiplier = 2, random = true), maxAttempts = 40)
  public VoucherNumber initiaizeInvoice(String uCode, String companyId, Long initialNumber)
  {
	  //companyId
	  //String.format("%06d", voucherCodeGenerator.next(uCode)) 
	  
	  return voucherNumberRepository.save(new VoucherNumber(uCode + String.format("%06d", voucherCodeGenerator.init(companyId+uCode,initialNumber)), companyId));
   // return voucherNumberRepository.save(new VoucherNumber(  uCode +String.format("%06d", voucherCodeGenerator.next(uCode))   ));
  }
  
  
}
