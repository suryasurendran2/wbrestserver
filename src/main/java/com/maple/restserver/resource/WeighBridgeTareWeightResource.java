package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.WeighBridgeTareWeight;
import com.maple.restserver.entity.WeighBridgeWeights;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.WeighBridgeTareWeightRepository;
@RestController
@Transactional
public class WeighBridgeTareWeightResource {
	@Autowired
	private WeighBridgeTareWeightRepository weighBridgeTareWeightRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/weighbridgetareweightbyvehicleno/{vehicleno}")
	public WeighBridgeTareWeight retrieveAllWeighBridgeTareWeight(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vehicleno") String vehicleno
			){
		return weighBridgeTareWeightRepository.findByVehicleno(vehicleno);
	}
	
	@GetMapping("{companymstid}/getalltareweight")
	public List<WeighBridgeTareWeight> getAllTareWeightOfVehicle(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "data") String data){
		data = data.replaceAll("20%", " ");
		
		return weighBridgeTareWeightRepository.searchByVehicleNo("%"+data.toLowerCase()+"%");
		
	}
	  
	
	
	@PostMapping("{companymstid}/saveweighbridgetareweight")
	public WeighBridgeTareWeight createWeighBridgeTareWeight(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  WeighBridgeTareWeight weighBridgeTareWeight)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			weighBridgeTareWeight.setCompanyMst(companyMst);
		
		
		return weighBridgeTareWeightRepository.save(weighBridgeTareWeight);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	
	

}
