package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.WeighingItemMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.WeighingItemMstRepository;

@RestController
@Transactional
public class WeighingItemMstResource {

	@Autowired
	WeighingItemMstRepository weighingItemMstRepo;


	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/weighingitemmst")
	public WeighingItemMst createWeighingItemMst(@Valid
			@RequestBody WeighingItemMst weighingItemMst,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			weighingItemMst.setCompanyMst(companyMst);
			 
			

			WeighingItemMst saved=weighingItemMstRepo.saveAndFlush(weighingItemMst);
		
	 	return saved;
	}
	@PutMapping("{companymstid}/weighingitemmst/{itemid}/updateitem")
	public WeighingItemMst updateItem(
			@PathVariable(value="itemid") String itemid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody WeighingItemMst  weighingItemMstRequest)
	{
			
		WeighingItemMst weighingItemMst = weighingItemMstRepo.findByCompanyMstIdAndItemId(companymstid, itemid);
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		weighingItemMst.setCompanyMst(companyMst);
		weighingItemMst.setBarcode(weighingItemMstRequest.getBarcode());
		weighingItemMst.setId(weighingItemMstRequest.getId());
		weighingItemMst.setItemId(itemid);
		return weighingItemMst;
	}
	@GetMapping("{companymstid}/weighingitemmst/{barcode}/weighingitembybarcode/{branchcode}")
	public WeighingItemMst getSalesReturnHdrById(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable("barcode") String barcode,
			  @PathVariable("branchcode") String branchcode)
	{
		return weighingItemMstRepo.findByCompanyMstIdAndBarcode(companymstid, barcode);
		
	}
	@GetMapping("{companymstid}/weighingitemmst/{itemcode}/weighingitembyitemid")
	public WeighingItemMst getItemByItemId(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable("itemcode") String itemcode )
	{
		return weighingItemMstRepo.findByCompanyMstIdAndItemId(companymstid, itemcode);
		
	}
}
