package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.WeighBridgeVehicleMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.WeighBridgeVehicleMstRepository;
@RestController
@Transactional
public class WeighBridgeVehicleMstResource {
	@Autowired
	private WeighBridgeVehicleMstRepository weighBridgeVehicleMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/findallweighbridgevehiclemst")
	public List<WeighBridgeVehicleMst> retrieveAllWeighBridgeVehicleMst(
			@PathVariable(value = "companymstid") String
			  companymstid){
		return weighBridgeVehicleMstRepository.findAll();
	}
	
	
	@GetMapping("{companymstid}/findweighbridgevehiclemstbyvehicletype/{vehicletype}")
	public WeighBridgeVehicleMst retrieveWeighBridgeVehicleMstByVelicleTyepe(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vehicletype") String vehicletype){
		return weighBridgeVehicleMstRepository.findByVehicletype(vehicletype);
	}
	  
	@GetMapping("{companymstid}/findweighbridgevehiclemstbyid/{id}")
	public Optional<WeighBridgeVehicleMst> retrieveWeighBridgeVehicleMstById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id){
		return weighBridgeVehicleMstRepository.findById(id);
	}
	  
	
	
	@PostMapping("{companymstid}/saveweighbridgevehiclemst")
	public WeighBridgeVehicleMst createWeighBridgeVehicleMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  WeighBridgeVehicleMst weighBridgeVehicleMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			weighBridgeVehicleMst.setCompanyMst(companyMst);
		
		
		return weighBridgeVehicleMstRepository.save(weighBridgeVehicleMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	
	

}
