package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class CompanyMst implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length = 10)
	private String id;
	
	@Column(length = 50)
	private String companyName;
	@Column(length = 30)
	private String state;
	@Column(length = 30)
	private String companyGst;
	@Column(length = 30)
	private String ipAdress;
	@Column(length = 20)
	String currencyName;
	@Column(length = 30)
	private String country;
	
	
	@CreationTimestamp
	@JsonIgnore
	private LocalDateTime updatedTime;
	 
	@Column(length = 50)
	String oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	public String getId() {
		return id;
	}
	 
	public String getCompanyName() {
		return companyName;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setCompanyName(String companyName) {
		 
		String idString = companyName.replaceAll("[^A-Za-z0-9]", "");
		 this.id=idString;
		
		this.companyName = companyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "CompanyMst [id=" + id + ", companyName=" + companyName + ", state=" + state + ", companyGst="
				+ companyGst + ", ipAdress=" + ipAdress + ", currencyName=" + currencyName + ", country=" + country
				+ ", updatedTime=" + updatedTime + ", oldId=" + oldId + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
}
