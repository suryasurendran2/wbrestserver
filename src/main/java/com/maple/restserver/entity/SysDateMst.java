package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class SysDateMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	@Column(length = 50)
	private String oldId;
   public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
private Date aplicationDate;
   private Date systemDate;
   
   @Column(length = 50)
   private String dayEndDone;
   @Column(length = 50)
   private String branchCode;

@ManyToOne(fetch = FetchType.EAGER, optional = false)
@JoinColumn(name = "companyMst", nullable = false)
@OnDelete(action = OnDeleteAction.CASCADE)
CompanyMst companyMst;
@Column(length = 50)
private String  processInstanceId;
@Column(length = 50)
private String taskId;

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public Date getAplicationDate() {
	return aplicationDate;
}
public void setAplicationDate(Date aplicationDate) {
	this.aplicationDate = aplicationDate;
}
public Date getSystemDate() {
	return systemDate;
}
public void setSystemDate(Date systemDate) {
	this.systemDate = systemDate;
}
public String getDayEndDone() {
	return dayEndDone;
}
public void setDayEndDone(String dayEndDone) {
	this.dayEndDone = dayEndDone;
}

public CompanyMst getCompanyMst() {
	return companyMst;
}
public void setCompanyMst(CompanyMst companyMst) {
	this.companyMst = companyMst;
}
public String getBranchCode() {
	return branchCode;
}
public void setBranchCode(String branchCode) {
	this.branchCode = branchCode;
}

public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "SysDateMst [id=" + id + ", oldId=" + oldId + ", aplicationDate=" + aplicationDate + ", systemDate="
			+ systemDate + ", dayEndDone=" + dayEndDone + ", branchCode=" + branchCode + ", companyMst=" + companyMst
			+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
}

}
