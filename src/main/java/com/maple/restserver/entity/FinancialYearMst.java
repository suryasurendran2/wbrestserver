

package com.maple.restserver.entity;




import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

 
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class  FinancialYearMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer id;
	@JsonProperty("username")
	@Column(length = 50)
	String UserName;
	Date startDate;
	Date endDate;
	@Column(length = 50)
	String financialYear;
	Integer currentFinancialYear;
	@Column(length = 50)
	String openCloseStatus;
	Integer oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public void setOldId(Integer oldId) {
		this.oldId = oldId;
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	public Integer getId() {
		return id;
	}
	
	//version2.0
		public Integer getCurrentFinancialYear() {
			return currentFinancialYear;
		}

		public void setCurrentFinancialYear(Integer currentFinancialYear) {
			this.currentFinancialYear = currentFinancialYear;
		}
	//version2.0ends
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getOpenCloseStatus() {
		return openCloseStatus;
	}

	public void setOpenCloseStatus(String openCloseStatus) {
		this.openCloseStatus = openCloseStatus;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public Integer getOldId() {
		return oldId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "FinancialYearMst [id=" + id + ", UserName=" + UserName + ", startDate=" + startDate + ", endDate="
				+ endDate + ", financialYear=" + financialYear + ", currentFinancialYear=" + currentFinancialYear
				+ ", openCloseStatus=" + openCloseStatus + ", oldId=" + oldId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + "]";
	}
	

}

