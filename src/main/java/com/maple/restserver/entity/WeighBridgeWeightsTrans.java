package com.maple.restserver.entity;

import java.io.Serializable;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class WeighBridgeWeightsTrans implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

 
	Integer firstWeight;
	
	@Column(length = 50)
	private String vehicleno;
	private Integer secondWeight;
	private Integer netWeight;
	
	
	@Column(length = 50)
	private String vehicletypeid;
	
	@Column(length = 50)
	private String materialtypeid;
	
	@Column(length = 50)
	private String firstweightdate;

	 
	private Integer rate;

 
	private LocalDateTime firstVoucherDate;
	
	@Column(length = 50)
	private String firstVoucherNumber;
	


	@Column(length = 50)
	private String secondVoucherNumber;
	
	 
	private LocalDateTime secondVoucherDate;

	

	@Column(length = 50)
	private String secondWeightDate;
	
	@UpdateTimestamp
	LocalDateTime updatedTime;


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	
	
	private String processInstanceId;
	@Column(length = 50)
	private String taskId;

	@JsonProperty("firstPaidAmount")
	private Integer firstPaidAmount;

	@JsonProperty("secondPaidAmount")
	private Integer secondPaidAmount;

	 

	@Column(length = 50)
	private String completionStatus;



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public Integer getFirstWeight() {
		return firstWeight;
	}



	public void setFirstWeight(Integer firstWeight) {
		this.firstWeight = firstWeight;
	}



	public String getVehicleno() {
		return vehicleno;
	}



	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}



	public Integer getSecondWeight() {
		return secondWeight;
	}



	public void setSecondWeight(Integer secondWeight) {
		this.secondWeight = secondWeight;
	}



	public Integer getNetWeight() {
		return netWeight;
	}



	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}



	public String getVehicletypeid() {
		return vehicletypeid;
	}



	public void setVehicletypeid(String vehicletypeid) {
		this.vehicletypeid = vehicletypeid;
	}



	public String getMaterialtypeid() {
		return materialtypeid;
	}



	public void setMaterialtypeid(String materialtypeid) {
		this.materialtypeid = materialtypeid;
	}



	public String getFirstweightdate() {
		return firstweightdate;
	}



	public void setFirstweightdate(String firstweightdate) {
		this.firstweightdate = firstweightdate;
	}



	public Integer getRate() {
		return rate;
	}



	public void setRate(Integer rate) {
		this.rate = rate;
	}



	public LocalDateTime getFirstVoucherDate() {
		return firstVoucherDate;
	}



	public void setFirstVoucherDate(LocalDateTime firstVoucherDate) {
		this.firstVoucherDate = firstVoucherDate;
	}



	public String getFirstVoucherNumber() {
		return firstVoucherNumber;
	}



	public void setFirstVoucherNumber(String firstVoucherNumber) {
		this.firstVoucherNumber = firstVoucherNumber;
	}



	public String getSecondVoucherNumber() {
		return secondVoucherNumber;
	}



	public void setSecondVoucherNumber(String secondVoucherNumber) {
		this.secondVoucherNumber = secondVoucherNumber;
	}



	public LocalDateTime getSecondVoucherDate() {
		return secondVoucherDate;
	}



	public void setSecondVoucherDate(LocalDateTime secondVoucherDate) {
		this.secondVoucherDate = secondVoucherDate;
	}



	public String getSecondWeightDate() {
		return secondWeightDate;
	}



	public void setSecondWeightDate(String secondWeightDate) {
		this.secondWeightDate = secondWeightDate;
	}



	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}



	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}



	public BranchMst getBranchMst() {
		return branchMst;
	}



	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public Integer getFirstPaidAmount() {
		return firstPaidAmount;
	}



	public void setFirstPaidAmount(Integer firstPaidAmount) {
		this.firstPaidAmount = firstPaidAmount;
	}



	public Integer getSecondPaidAmount() {
		return secondPaidAmount;
	}



	public void setSecondPaidAmount(Integer secondPaidAmount) {
		this.secondPaidAmount = secondPaidAmount;
	}



	public String getCompletionStatus() {
		return completionStatus;
	}



	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}





}
