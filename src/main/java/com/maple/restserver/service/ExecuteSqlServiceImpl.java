package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.hibernate.Session;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.ExecuteSql;
import com.maple.restserver.entity.ExecuteSqlCmdMst;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.ExecuteSqlCmdMstRepository;
import com.maple.restserver.repository.ExecuteSqlRepository;

@Service
@Transactional
@Component
public class ExecuteSqlServiceImpl implements ExecuteSqlService {

	@Autowired
	ExecuteSqlCmdMstRepository executeSqlCmdMstRepository;

	@Autowired
	ExecuteSqlRepository executeSqlRepository;

	@Autowired
	BranchMstRepository branchMstRepository;

	@PersistenceContext
	private EntityManager em;

	@Override
	public void executeSqlCommand() {
		BranchMst branchMst = branchMstRepository.getMyBranch();

		List<ExecuteSqlCmdMst> executeSqlCmdMstList = executeSqlCmdMstRepository.findByExecuteStatus("NO");
		for (ExecuteSqlCmdMst executeSqlCmdMst : executeSqlCmdMstList) {
			executeSql(executeSqlCmdMst.getSqlCommand(), branchMst);
			executeSqlCmdMst.setExecuteStatus("YES");

			executeSqlCmdMstRepository.save(executeSqlCmdMst);

		}
	}

	@Override
	public void insertSqlCommand(String id, String Sql) {

		ExecuteSqlCmdMst executeSqlCmdMst = new ExecuteSqlCmdMst(id, Sql, "NO");
		executeSqlCmdMstRepository.save(executeSqlCmdMst);

	}

	@Override
	public List<Map<String, Object>> executeSql(String executeSqlParam, BranchMst branchMst) {
		executeSqlParam.replaceAll("%20", " ");

		Session session = em.unwrap(Session.class);

		ExecuteSql executeSql = new ExecuteSql();
		executeSql.setBrachCode(branchMst.getBranchCode());
		executeSql.setCompanyMst(branchMst.getCompanyMst());
		executeSql.setExecuteInServer(false);
		executeSql.setSqlToExecute(executeSqlParam);
		executeSqlRepository.save(executeSql);

		if (executeSqlParam.toUpperCase().contains("UPDATE") || executeSqlParam.toUpperCase().contains("DELETE")
				|| executeSqlParam.toUpperCase().contains("CREATE") || executeSqlParam.toUpperCase().contains("ALTER")
				|| executeSqlParam.toUpperCase().contains("INSERT") || executeSqlParam.toUpperCase().contains("DROP")) {
			Query query = session.createSQLQuery(executeSqlParam);

			int result = query.executeUpdate();

		} else {
			// executeSql = executeSql +" fetch first 100 rows only " ;
			Query query = session.createSQLQuery(executeSqlParam);
			((org.hibernate.query.Query) query).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			List<Map<String, Object>> aliasToValueMapList = ((org.hibernate.query.Query) query).list();
			
			session.close();
			return aliasToValueMapList;

		}
		session.close();
		return null;

	}

}
