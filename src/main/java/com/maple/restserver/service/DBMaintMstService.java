package com.maple.restserver.service;


import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
@Component
public interface DBMaintMstService {
	void insertDBmaintmst(String id, String  Sql,String execStatus);
	void executeDBmaintmst();
}
