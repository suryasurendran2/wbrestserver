package com.maple.restserver.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DBMaintMst;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DBMaintMstRepository;

@Service
@Transactional
@Component
public class DBMaintMstServiceImpl implements DBMaintMstService {


	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	DBMaintMstRepository dbMaintMstRepository;

	@Autowired
	ExecuteSqlService executeSqlService;

	@Autowired
	BranchMstRepository branchMstRepository;
	
	@Value("${mycompany}")
	private String mycompany;
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Override
	public void insertDBmaintmst(String id, String sqlString, String execStatus) {

		DBMaintMst dbMaintMst = new DBMaintMst();
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(mycompany);
		if(!companyMstOpt.isPresent()) {
		
		return;
		}
		CompanyMst companyMst= companyMstOpt.get();
		Optional<DBMaintMst> dbMaintMstOpt = dbMaintMstRepository.findById(id);
		dbMaintMst = null;
		if(dbMaintMstOpt.isPresent())
		{
			dbMaintMst = dbMaintMstOpt.get();

		}
		if (null == dbMaintMst) {
			dbMaintMst = new DBMaintMst();
			dbMaintMst.setId(id);
			dbMaintMst.setBranchCode(mybranch);
			dbMaintMst.setCompanyMst(companyMst);
			dbMaintMst.setSqlString(sqlString);
			dbMaintMst.setExecStatus("NO");
			dbMaintMst = dbMaintMstRepository.saveAndFlush(dbMaintMst);
			System.out.println(dbMaintMst);
			return;
		} else {
			return;
		}

	}

	@Override
	public void executeDBmaintmst() {

		List<DBMaintMst> dbMaintMstList = dbMaintMstRepository.findByExecStatus("NO");
		for (DBMaintMst dbMaintMst : dbMaintMstList) {

			List<BranchMst> branchMst = branchMstRepository.findByBranchCode(dbMaintMst.getBranchCode());
			if (branchMst.size() > 0) {
				executeSqlService.executeSql(dbMaintMst.getSqlString(), branchMst.get(0));

				dbMaintMst.setExecStatus("YES");
				dbMaintMstRepository.save(dbMaintMst);
			}

		}

	}

}
