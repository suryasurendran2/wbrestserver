package com.maple.maple.util;

import java.math.BigDecimal;
import java.util.HashMap;

public class GSTCalculator implements TaxCalculater{

	@Override
	public BigDecimal execute(HashMap map) {
		

		Double percet =(Double) map.get("TAXPERCENT");
		Double MRP =(Double) map.get("MRP");
		
		BigDecimal bd = new BigDecimal(MRP.doubleValue()*percet.doubleValue()/100);
		
		
		
		return bd;
	}

}
